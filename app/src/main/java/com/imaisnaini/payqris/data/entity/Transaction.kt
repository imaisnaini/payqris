package com.imaisnaini.payqris.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.time.LocalDate
import java.util.Date
import java.util.UUID

@Entity(tableName = "t_transaction")
data class Transaction(
    @PrimaryKey @ColumnInfo("id") val id: UUID = UUID.randomUUID(),
    @ColumnInfo("bank") val bank: String,
    @ColumnInfo("trx_id") val trxId: String,
    @ColumnInfo("merchant_name") val merchantName: String,
    @ColumnInfo("nominal") val nominal: Float,
    @ColumnInfo("created_at") val createdAt:LocalDate = LocalDate.now()
)

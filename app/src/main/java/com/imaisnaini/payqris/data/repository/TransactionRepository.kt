package com.imaisnaini.payqris.data.repository

import com.imaisnaini.payqris.data.PagingResult
import com.imaisnaini.payqris.data.SingleResult
import com.imaisnaini.payqris.data.dao.TransactionDao
import com.imaisnaini.payqris.data.dao.UserDao
import com.imaisnaini.payqris.data.entity.Transaction
import com.imaisnaini.payqris.data.entity.User
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import timber.log.Timber
import java.time.LocalDate
import java.util.UUID
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TransactionRepository @Inject constructor(private  val transactionDao: TransactionDao,
private val userDao: UserDao){

    fun findByID(id: UUID):SingleResult<Transaction>{
        val toReturn = SingleResult<Transaction>()
        try {
            val trx = transactionDao.getTransaction(id)
            toReturn.record = trx
            toReturn.success = true
        }catch (e: Exception){
            Timber.e(e)
            toReturn.success = false
            toReturn.message = e.message
        }
        return toReturn
    }

    fun findAll():PagingResult<Transaction>{
        val toReturn = PagingResult<Transaction>()
        try {
            val trx = transactionDao.getTransactions()
            toReturn.records = trx
            toReturn.success = true
        }catch (e: Exception){
            Timber.e(e)
            toReturn.success = false
            toReturn.message = e.message
        }
        return toReturn
    }

    fun pay(transaction: Transaction, user: User): SingleResult<Transaction>{
        val toReturn = SingleResult<Transaction>()
        try {
            var userToSave = user
            userToSave.saldo = user.saldo.minus(transaction.nominal)
            userToSave.updatedAt = LocalDate.now()
            transactionDao.insert(transaction)
            userDao.update(userToSave)
            val trx = transactionDao.getTransaction(transaction.id)
            toReturn.record = trx
            toReturn.success = true
        }catch (e: Exception){
            Timber.e(e)
            toReturn.success = false
            toReturn.message = e.message
        }
        return toReturn
    }

    companion object {
        // For Singleton instantiation
        @Volatile private var instance: TransactionRepository? = null

        fun getInstance(transactionDao: TransactionDao, userDao: UserDao) =
            instance ?: synchronized(this) {
                instance ?: TransactionRepository(transactionDao, userDao).also { instance = it }
            }
    }
}
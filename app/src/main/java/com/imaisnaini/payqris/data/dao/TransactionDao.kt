package com.imaisnaini.payqris.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.imaisnaini.payqris.data.SingleResult
import com.imaisnaini.payqris.data.entity.Transaction
import kotlinx.coroutines.flow.Flow
import java.util.UUID

@Dao
interface TransactionDao {
    @Query("SELECT * FROM t_transaction ORDER BY created_at")
    fun getTransactions(): List<Transaction>

    @Query("SELECT * FROM t_transaction WHERE id = :id")
    fun getTransaction(id: UUID): Transaction

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(transactions: List<Transaction>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(transactions: Transaction)
}
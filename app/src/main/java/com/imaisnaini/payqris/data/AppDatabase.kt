/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.imaisnaini.payqris.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.imaisnaini.payqris.data.converter.BooleanConverter
import com.imaisnaini.payqris.data.converter.DateConverter
import com.imaisnaini.payqris.data.converter.LocalDateConverter
import com.imaisnaini.payqris.data.converter.UuidConverter
import com.imaisnaini.payqris.data.dao.TransactionDao
import com.imaisnaini.payqris.data.dao.UserDao
import com.imaisnaini.payqris.data.entity.Transaction
import com.imaisnaini.payqris.data.entity.User

/**
 * The Room database for this app
 */
@Database(entities = [Transaction::class, User::class], version = 1, exportSchema = false)
@TypeConverters(UuidConverter::class, DateConverter::class, BooleanConverter::class, LocalDateConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun transactionDao(): TransactionDao
    abstract fun userDao(): UserDao

    companion object {
        const val DATABASE_NAME = "payqris"
        @Volatile
        private var INSTANCE: AppDatabase? = null

        // For Singleton instantiation
        @Volatile private var instance: AppDatabase? = null

//        fun getInstance(context: Context): AppDatabase {
//            return instance ?: synchronized(this) {
//                instance ?: buildDatabase(context).also { instance = it }
//            }
//        }

        // Create and pre-populate the database. See this article for more details:
        // https://medium.com/google-developers/7-pro-tips-for-room-fbadea4bfbd1#4785
//        private fun buildDatabase(context: Context): AppDatabase {
//            return Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
//                .addCallback(
//                    object : RoomDatabase.Callback() {
//                        override fun onCreate(db: SupportSQLiteDatabase) {
//                            super.onCreate(db)
//                            val request = OneTimeWorkRequestBuilder<SeedDatabaseWorker>()
//                                    .setInputData(workDataOf(KEY_FILENAME to PLANT_DATA_FILENAME))
//                                    .build()
//                            WorkManager.getInstance(context).enqueue(request)
//                        }
//                    }
//                )
//                .build()
//        }

        fun getInstance(context: Context): AppDatabase? {
            if (AppDatabase.INSTANCE == null) {
                synchronized(AppDatabase::class.java) {
                    if (AppDatabase.INSTANCE == null) {
                        val builder =
                            Room.databaseBuilder(
                                context.applicationContext,
                                AppDatabase::class.java, DATABASE_NAME
                            )
                                .addCallback(sRoomDatabaseCallback)
//                        if (!BuildConfig.DEBUG) {
//                            try {
//                                val passphrase: ByteArray = AppDatabase.getPassphrase(context)
//                                builder.openHelperFactory(SupportFactory(passphrase))
//                            } catch (exc: GeneralSecurityException) {
//                                throw RuntimeException(
//                                    "Failed while preparing DB encryption",
//                                    exc
//                                )
//                            } catch (exc: IOException) {
//                                throw RuntimeException(
//                                    "Failed while preparing DB encryption",
//                                    exc
//                                )
//                            }
//                        }
                        AppDatabase.INSTANCE = builder.build()
                    }
                }
            }
            return AppDatabase.INSTANCE
        }

        private val sRoomDatabaseCallback: Callback = object : Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
            }
        }
    }
}

package com.imaisnaini.payqris.data.repository

import com.imaisnaini.payqris.data.SingleResult
import com.imaisnaini.payqris.data.dao.UserDao
import com.imaisnaini.payqris.data.entity.User
import timber.log.Timber
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository @Inject constructor(private val userDao: UserDao){
    fun findByID(id: UUID): SingleResult<User> {
        val toReturn = SingleResult<User>();
        try {
            val user = userDao.getUser(id)
            toReturn.record = user
            toReturn.success = true
        }catch (e: Exception){
            Timber.e(e)
            toReturn.success = false
            toReturn.message = e.message
        }
        return toReturn
    }
    fun login(username: String, pin: Int): SingleResult<User> {
        val toReturn = SingleResult<User>();
        try {
            val user = userDao.login(username, pin)
            toReturn.record = user
            toReturn.success = true
        }catch (e: Exception){
            Timber.e(e)
            toReturn.success = false
            toReturn.message = e.message
        }
        return toReturn
    }
    
    fun register(usr: User): SingleResult<User>{
        val toReturn = SingleResult<User>();
        try {
            userDao.insert(usr)
            val user = userDao.getUser(usr.id)
            toReturn.record = user
            toReturn.success = true
        }catch (e: Exception){
            Timber.e(e)
            toReturn.success = false
            toReturn.message = e.message
        }
        return toReturn
    }

    companion object {
        // For Singleton instantiation
        @Volatile private var instance: UserRepository? = null

        fun getInstance(userDao: UserDao) =
            instance ?: synchronized(this) {
                instance ?: UserRepository(userDao).also { instance = it }
            }
    }
}
package com.imaisnaini.payqris.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.imaisnaini.payqris.data.entity.Transaction
import com.imaisnaini.payqris.data.entity.User
import java.util.*

@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: User)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun update(user: User)

    @Query("SELECT * FROM t_user WHERE id = :id")
    fun getUser(id: UUID): User

    @Query("SELECT * FROM t_user WHERE username=:username AND pin=:pin LIMIT 0,1")
    fun login(username:String, pin:Int): User
}
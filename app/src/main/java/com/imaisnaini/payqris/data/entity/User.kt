package com.imaisnaini.payqris.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.time.LocalDate
import java.util.*

@Entity(tableName = "t_user")
data class User(
    @PrimaryKey @ColumnInfo("id") val id: UUID = UUID.randomUUID(),
    @ColumnInfo("fulllname") val fullname: String,
    @ColumnInfo("username") val username: String,
    @ColumnInfo("rekening") val rekening: String,
    @ColumnInfo("saldo") var saldo: Float = 1000000f,
    @ColumnInfo("pin") val pin: Int = 123456,
    @ColumnInfo("created_at") val createdAt: LocalDate = LocalDate.now(),
    @ColumnInfo("updated_at") var updatedAt: LocalDate = LocalDate.now()
    )

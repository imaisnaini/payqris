package com.imaisnaini.payqris.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.*

private val DarkColorPalette = darkColors(
    primary = Orange_700,
    onPrimary = Ghost_White,
    primaryVariant = Orange_100,
    secondary = Turquoise_700,
    secondaryVariant = Turquoise_300,
    onSecondary = White,
    surface = Night,
    onSurface = Ghost_White,
)

private val LightColorPalette = lightColors(
    primary = Orange_700,
    onPrimary = Ghost_White,
    primaryVariant = Orange_100,
    secondary = Turquoise_700,
    secondaryVariant = Turquoise_300,
    onSecondary = White,
    surface = Ghost_White,
    onSurface = Night,
)

@Composable
fun PayQrisTheme(darkTheme: Boolean = isSystemInDarkTheme(), content: @Composable () -> Unit) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }
    MaterialTheme(
        colors = colors,
        typography = Typography,
        content = content
    )
}
package com.imaisnaini.payqris.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.imaisnaini.payqris.CommonViewModel
import com.imaisnaini.payqris.R
import com.imaisnaini.payqris.data.entity.User
import com.imaisnaini.payqris.ui.components.Password
import com.imaisnaini.payqris.ui.components.PasswordState
import com.imaisnaini.payqris.ui.compose.RequiredState
import com.imaisnaini.payqris.ui.theme.*
import dagger.hilt.android.AndroidEntryPoint
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import kotlin.random.Random

@AndroidEntryPoint
class LoginFragment: Fragment() {
    val viewModel: CommonViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                val isLoginMode = remember {
                    mutableStateOf(true)
                }
                viewModel.userLiveData.observe(viewLifecycleOwner){
                    if (it != null){
                        findNavController().navigate(R.id.nav_graph)
                    }
                }
                PayQrisTheme {
                    Surface(color = MaterialTheme.colors.surface) {
                        Box {
                            Surface(
                                modifier = Modifier
                                    .fillMaxSize()
                                    .systemBarsPadding()
                            )
                            {
                                Column(modifier = Modifier
                                    .fillMaxSize()
                                    .background(
                                        brush = Brush.verticalGradient(
                                            colors = listOf(Turquoise_100, Turquoise_700)
                                        )
                                    )){
                                    Column(verticalArrangement = Arrangement.Center,
                                        modifier = Modifier
                                            .fillMaxSize()
                                            .padding(all = padding_large)) {
                                        Row(horizontalArrangement = Arrangement.Center,
                                        verticalAlignment = Alignment.CenterVertically,
                                        modifier = Modifier.fillMaxWidth()) {
                                            Text(text = "Pay", style = Typography.h3, fontWeight = FontWeight.Bold)
                                            Image(painter = painterResource(id = R.drawable.ic_qris_logo), contentDescription = "",
                                                modifier = Modifier.height(img_small))
                                        }
                                        if (isLoginMode.value){
                                            loginScreen(isLoginMode)
                                        }else{
                                            registercreen(isLoginMode)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Composable
    fun loginScreen(isLoginMode: MutableState<Boolean> = remember {
        mutableStateOf(true)
    }){
        val passwordState = remember { PasswordState() }
        val usernameState  = remember { RequiredState() }
        val onSubmit = {
            if (usernameState.text.trim().isEmpty()){
                usernameState.setError("Username wajib diisi")
//                Toast.makeText(context, "Password wajib diisi", Toast.LENGTH_SHORT).show()
            }
            if (passwordState.text.trim().isEmpty()){
                passwordState.setError("Password wajib diisi")
//                Toast.makeText(context, "Password wajib diisi", Toast.LENGTH_SHORT).show()
            }
            if (passwordState.isValid && usernameState.isValid) {
                viewModel.login(usernameState.text, passwordState.text.toInt())
            }
        }
        Card(modifier = Modifier.fillMaxWidth()) {
            Column(horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(all = padding_large)
            ) {
                OutlinedTextField(value = usernameState.text,
                    onValueChange = { usernameState.text = it },
                    label = {Text(text = "Username")},
                    modifier = Modifier.fillMaxWidth()
                )
                Password(label = "PIN", passwordState = passwordState, onImeAction = onSubmit)
                Text("${viewModel.getErrorMsg().observeAsState().value}", color = Color.Red,
                modifier = Modifier.padding(vertical = padding_small))
                Spacer(modifier = Modifier.height(spacer_large))
                Button(onClick = { onSubmit() }) {
                    Text(text = "LOGIN", style = Typography.button)
                }
                Text(text = "I don't have account. Register ->", fontStyle = FontStyle.Italic,
                    modifier = Modifier
                        .padding(all = padding_normal)
                        .clickable { isLoginMode.value = false })
            }
        }
    }

    @Composable
    fun registercreen(isLoginMode: MutableState<Boolean> = remember {
        mutableStateOf(false)
    }){
        val formatter = DateTimeFormatter.ofPattern("yyyyMMdd")
        val pinState = remember { PasswordState() }
        val usernameState  = remember { RequiredState() }
        val fulnameState  = remember { RequiredState() }
        val onSubmit = {
            if (usernameState.text.trim().isEmpty()){
                usernameState.setError("Username wajib diisi")
            }
            if (fulnameState.text.trim().isEmpty()){
                fulnameState.setError("Nama lengkap wajib diisi")
            }
            if (pinState.text.trim().isEmpty()){
                pinState.setError("Pin wajib diisi")
            }
            if (usernameState.isValid && fulnameState.isValid && pinState.isValid) {
                val rek = formatter.format(LocalDate.now()) + Random.nextInt(10, 99).toString()
                var user = User(
                    fullname = fulnameState.text,
                    username = usernameState.text,
                    pin = pinState.text.toInt(),
                    rekening = rek
                )
                viewModel.register(user)
            }
        }
        Card(modifier = Modifier.fillMaxWidth()) {
            Column(horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(all = padding_large)
            ) {
                OutlinedTextField(value = fulnameState.text,
                    onValueChange = { fulnameState.text = it },
                    label = {Text(text = "Full Name")},
                    modifier = Modifier.fillMaxWidth()
                )
                OutlinedTextField(value = usernameState.text,
                    onValueChange = { usernameState.text = it },
                    label = {Text(text = "Username")},
                    modifier = Modifier.fillMaxWidth()
                )
                Password(label = "PIN", passwordState = pinState, onImeAction = onSubmit)
                Spacer(modifier = Modifier.height(spacer_large))
                Button(onClick = { onSubmit() }) {
                    Text(text = "REGISTER", style = Typography.button)
                }
                Text(text = "I already have account. Back to Login ->", fontStyle = FontStyle.Italic,
                    modifier = Modifier
                        .padding(all = padding_normal)
                        .clickable { isLoginMode.value = true })
            }
        }
    }
}
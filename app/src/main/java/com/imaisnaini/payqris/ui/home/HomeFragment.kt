package com.imaisnaini.payqris.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Sync
import androidx.compose.material.icons.filled.WavingHand
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.imaisnaini.payqris.CommonViewModel
import com.imaisnaini.payqris.R
import com.imaisnaini.payqris.data.entity.User
import com.imaisnaini.payqris.ui.theme.*
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment() {
    val viewModel: CommonViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                viewModel.userLiveData.observe(viewLifecycleOwner){
                    if (it == null){
                        findNavController().navigate(R.id.home_to_login)
                    }
                }
                PayQrisTheme {
                    Surface(color = MaterialTheme.colors.surface) {
                        Box {
                            Surface(
                                modifier = Modifier
                                    .fillMaxSize()
                                    .statusBarsPadding()
                                    .imePadding()
                            )
                            {
                                Column {
                                    TopBar()
                                    Column(modifier = Modifier
                                        .fillMaxSize()
                                        .background(
                                            brush = Brush.verticalGradient(
                                                colors = listOf(Turquoise_100, Turquoise_700)
                                            )
                                        )) {
                                        viewModel.userLiveData.observeAsState().value?.let {
                                            Greetings(it)
                                            Profile(it)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    @Composable
    private fun TopBar(){
        TopAppBar (
            title = { Text("PayQRIS") },
            actions ={ IconButton(onClick = { viewModel.refresh() }) {
                Icon(imageVector = Icons.Filled.Sync, "Icon Button Sync")
            }
            }
        )
    }

    @Composable
    fun Greetings(user: User){
        Row(verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .fillMaxWidth()
                .padding(padding_large)) {
            Text(text = "Hello, ${user.fullname}", style = Typography.h6)
            Image(painter = painterResource(id = R.drawable.ic_waving_hand), contentDescription = "",
            modifier = Modifier
                .size(icon_medium)
                .padding(horizontal = padding_small))
        }
    }
    
    @Composable
    fun Profile(user: User){
        Card(shape = Shapes.medium,
            elevation = elevation_normal,
            modifier = Modifier.padding(all = padding_normal)
        ) {
            Column(modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = padding_large, horizontal = padding_normal))
            {
                Text(text = "BNI - ${user.rekening}", style = Typography.subtitle2)
                Divider(modifier = Modifier.padding(vertical = padding_small))
                Text(text = "Rp. ${user.saldo}", style = Typography.h5,
                    modifier = Modifier.padding(vertical = padding_normal))
            }
        }
    }
}
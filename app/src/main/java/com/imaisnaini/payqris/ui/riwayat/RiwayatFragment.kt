package com.imaisnaini.payqris.ui.riwayat

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Sync
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.text.font.FontStyle
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.imaisnaini.payqris.data.entity.Transaction
import com.imaisnaini.payqris.ui.qris.QrisViewModel
import com.imaisnaini.payqris.ui.theme.*
import dagger.hilt.android.AndroidEntryPoint
import java.time.format.DateTimeFormatter

@AndroidEntryPoint
class RiwayatFragment: Fragment() {
    val viewModel: RiwayatViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel.findAll()

        return ComposeView(requireContext()).apply {
            setContent {
                val trxList = viewModel.trxLiveData.observeAsState()
                PayQrisTheme {
                    Surface(color = MaterialTheme.colors.surface) {
                        Box {
                            Surface(
                                modifier = Modifier
                                    .fillMaxSize()
                                    .statusBarsPadding()
                                    .imePadding()
                            )
                            {
                                Column {
                                    TopBar()
                                    Column(horizontalAlignment = Alignment.CenterHorizontally,
                                        modifier = Modifier
                                        .fillMaxSize()
                                        .background(
                                            brush = Brush.verticalGradient(
                                                colors = listOf(Turquoise_100, Turquoise_700)
                                            )
                                        )) {
                                        if (trxList.value.isNullOrEmpty()){
                                            Text("No Records of Transaction", fontStyle = FontStyle.Italic,
                                            modifier = Modifier.padding(all = padding_large))
                                        }else {
                                            LazyColumn {
                                                items(trxList.value!!) {
                                                    TransactionItem(data = it)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    @Composable
    private fun TopBar(){
        TopAppBar (
            title = { Text("Riwayat") },
            actions ={ IconButton(onClick = {
                viewModel.findAll()
            }) {
                Icon(imageVector = Icons.Filled.Sync, "Icon Button Sync")
            }
            }
        )
    }

    @Composable
    fun TransactionItem(data: Transaction){
        val formatter = DateTimeFormatter.ofPattern("dd MMMM yyyy")
        Card(shape = Shapes.medium,
            elevation = elevation_normal,
            modifier = Modifier.padding(all = padding_normal)
        ) {
            Row(verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = padding_large, horizontal = padding_normal))
            {
                Column(modifier = Modifier.weight(1f, true)) {
                    Text(text = "${data.merchantName}", style = Typography.subtitle2)
                    Text(text = "${formatter.format(data.createdAt)}")
                }
                Text(text = "Rp. ${data.nominal}", style = Typography.h6,
                    modifier = Modifier.padding(vertical = padding_normal))
            }
        }
    }
}
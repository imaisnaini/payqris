package com.imaisnaini.payqris.ui.qris

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.imaisnaini.payqris.data.entity.Transaction
import com.imaisnaini.payqris.data.entity.User
import com.imaisnaini.payqris.data.repository.TransactionRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import timber.log.Timber
import java.util.*
import javax.inject.Inject

@HiltViewModel
class QrisViewModel @Inject constructor(private val transactionRepository: TransactionRepository): ViewModel() {
    var qrisLiveData = MutableLiveData<String>("")
    fun getQrisdLiveData() = qrisLiveData
    fun setQrisLiveData(value: String){
        qrisLiveData.value = value
    }

    var trxLiveData = MutableLiveData<Transaction>()

    var trxSuccessEvent = MutableLiveData<Boolean?>(null)
    fun dismissTrxSuccessEvent(){trxSuccessEvent.value = null}

    var errorMessage = MutableLiveData<String?>()
    fun resetMessage() {errorMessage.value = null}

    fun isQRValid(qr: String): Boolean{
        var qrValues = qr.split(".")
        if (qrValues.size == 4){
            qrisLiveData.value = qr
            var trx = Transaction(UUID.randomUUID(), qrValues[0], qrValues[1], qrValues[2], qrValues[3].toFloat())
            trxLiveData.value = trx
            return true
        }else{
            errorMessage.value = "QR Format Invalid"
            return false
        }
    }

    fun pay(pin:Int, user: User){
        var transaction = trxLiveData.value!!
        if (pin.equals(user.pin)){
            if (user.saldo < transaction.nominal){
                errorMessage.value = "Insufficient Balance"
            }else{
                Single.fromCallable { transactionRepository.pay(transaction, user) }
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe({
                        if (it.success) {
                            trxLiveData.postValue(it.record)
                            trxSuccessEvent.postValue(true)
                        }else{
                            errorMessage.postValue(it.message)
                        }
                    }, { t: Throwable? -> Timber.e(t) })
            }
        }else{
            errorMessage.value = "Invalid PIN"
        }
    }
}
package com.imaisnaini.payqris.ui.theme

import androidx.compose.ui.unit.dp

val padding_small = 4.dp
val padding_normal = 8.dp
val padding_large = 16.dp

val spacer_small = 16.dp
val spacer_normal = 24.dp
val spacer_large = 32.dp

val radius_small = 4.dp
val radius_normal = 10.dp
val radius_large = 16.dp
val radius_xlarge = 50.dp

val border_light = 1.dp
val border_normal = 2.dp

val elevation_light = 2.dp
val elevation_normal = 4.dp
val elevation_high = 6.dp

val icon_xsmall = 16.dp
val icon_small = 24.dp
val icon_medium = 36.dp
val icon_large = 48.dp
val icon_xlarge = 60.dp

val img_xsmall = 75.dp
val img_small = 100.dp
val img_medium = 150.dp
val img_large = 200.dp
val img_xlarge = 300.dp
package com.imaisnaini.payqris.ui.qris

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBackIos
import androidx.compose.material.icons.filled.ChevronLeft
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.imaisnaini.payqris.CommonViewModel
import com.imaisnaini.payqris.R
import com.imaisnaini.payqris.data.entity.Transaction
import com.imaisnaini.payqris.data.entity.User
import com.imaisnaini.payqris.ui.components.Password
import com.imaisnaini.payqris.ui.components.PasswordState
import com.imaisnaini.payqris.ui.theme.*
import dagger.hilt.android.AndroidEntryPoint
import org.acra.util.Installation.id

@AndroidEntryPoint
class PinFragment: Fragment() {
    val viewModel: QrisViewModel by activityViewModels()
    val cViewModel: CommonViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return ComposeView(requireContext()).apply {
            setContent {
                val passwordState = remember { PasswordState() }
                val onSubmit = {
                    if (passwordState.text.trim().isEmpty()){
                        passwordState.setError("Password wajib diisi")
                        Toast.makeText(context, "Password wajib diisi", Toast.LENGTH_SHORT).show()
                    }
                    if (passwordState.isValid) {
                        viewModel.pay(passwordState.text.toInt(), cViewModel.userLiveData.value!!)
                    }
                }
                val showDialog =  remember { mutableStateOf(false) }
                viewModel.trxSuccessEvent.observe(viewLifecycleOwner){
                    if (it == true){
                        showDialog.value = true
                        viewModel.dismissTrxSuccessEvent()
                    }
                }
                viewModel.errorMessage.observe(viewLifecycleOwner){
                    if (!it.isNullOrEmpty()){
                        Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
                        viewModel.resetMessage()
                    }
                }
                PayQrisTheme {
                    // A surface container using the 'background' color from the theme
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colors.background
                    ) {
                        Column(modifier = Modifier.fillMaxSize()) {
                            TopAppBar()
                            if (showDialog.value){
                                viewModel.trxLiveData.observeAsState().value?.let {
                                    cViewModel.userLiveData.observeAsState().value?.let {
                                            it1 -> DialogSuccess(transaction = it, user = it1) } }
                            }
                            Column(verticalArrangement = Arrangement.Center,
                                horizontalAlignment = Alignment.CenterHorizontally,
                                modifier = Modifier
                                    .fillMaxSize()
                                    .padding(all = padding_large)) {
                                Text(text = "Masukkan PIN", style = Typography.h5)
                                Spacer(modifier = Modifier.height(spacer_normal))
                                Password(label = "", passwordState = passwordState, onImeAction = onSubmit)
                                Spacer(modifier = Modifier.height(spacer_large))
                                Button(onClick = { onSubmit() },
                                    colors = ButtonDefaults.buttonColors(backgroundColor = Turquoise_500),
                                    modifier = Modifier
                                        .padding(horizontal = padding_normal)
                                ) {
                                    Text("BAYAR", color = Ghost_White)
                                }
                            }

                        }
                    }
                }
            }
        }
    }

    @OptIn(ExperimentalFoundationApi::class)
    @Composable
    private fun TopAppBar() {
        TopAppBar(
            title = {
                Text(
                    text = ""
                )
            },
            backgroundColor = Color.Transparent,
            contentColor = Orange_700,
            navigationIcon = {
                IconButton(onClick = { findNavController().navigateUp() }) {
                    Icon(imageVector = Icons.Filled.ArrowBackIos, "Icon Button Back")
                }
            },
            elevation = 0.dp
        )
    }

    @Composable
    fun DialogSuccess(dialogState: MutableState<Boolean> = remember {
        mutableStateOf(false)
    }, transaction: Transaction, user: User){
        Dialog(onDismissRequest = { /*TODO*/ }) {
            Surface (
                shape = RoundedCornerShape(radius_normal)
            ) {
                Box {
                    Column(modifier = Modifier.padding(all = padding_normal)) {
                        Column(
                            horizontalAlignment = Alignment.CenterHorizontally,
                            modifier = Modifier
                                .fillMaxWidth()
                                .weight(1f, false)
                        ) {
                            Image(painter = painterResource(id = R.drawable.ic_checked), contentDescription = "success",
                            modifier = Modifier
                                .size(img_xlarge)
                                .padding(all = padding_large))
                            Text(text = "Transaksi sebesar ${transaction.nominal} berhasil", style = Typography.subtitle2)
                            Spacer(modifier = Modifier.height(spacer_normal))
                            Text(text = "Saldo Anda", style = Typography.subtitle1)
                            Text(text = "${user.saldo}", style = Typography.h4)
                        }
                        Row(horizontalArrangement = Arrangement.End,
                            verticalAlignment = Alignment.CenterVertically,
                            modifier = Modifier.fillMaxWidth()){
                            Button(onClick = {
                                findNavController().navigate(R.id.navHomeFragment)
                            }) {
                                Text("TUTUP")
                            }
                        }
                    }
                }
            }
        }
    }
}
package com.imaisnaini.payqris.ui.theme

import androidx.compose.ui.graphics.Color

val Black = Color(0xFF000000)
val Night = Color(0xFF0D0A0B)
val White = Color(0xFFFFFFFF)
val Ghost_White = Color(0xFFFBFBFF)
val Grey_100 = Color(0xFFF5F4EE)
val Grey_200 = Color(0xFFDED9C4)
val Grey_400 = Color(0xFFBCBCBC)
val Grey_600 = Color(0xFFAAAAAA)
val Grey_700 = Color(0xFF777777)
val Orange_100 = Color(0xFFFFCC80)
val Orange_400 = Color(0xFFFFA726)
val Orange_700 = Color(0xFFFF6600)
val Turquoise_100 = Color(0xFFBBDEFB)
val Turquoise_300 = Color(0xFF7986CB)
val Turquoise_500 = Color(0xFF5C6BC0)
val Turquoise_700 = Color(0xFF006699)
val Transparent = Color(0x00FFFFFF)
val Opacity = Color(0xB2000000)
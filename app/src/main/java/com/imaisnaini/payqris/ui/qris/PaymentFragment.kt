package com.imaisnaini.payqris.ui.qris

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBackIos
import androidx.compose.material.icons.filled.ArrowCircleLeft
import androidx.compose.material.icons.filled.ChevronLeft
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.imaisnaini.payqris.CommonViewModel
import com.imaisnaini.payqris.R
import com.imaisnaini.payqris.ui.theme.*
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PaymentFragment: Fragment() {
    val viewModel: QrisViewModel by activityViewModels()
    val cViewModel: CommonViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View{

        return ComposeView(requireContext()).apply {
            setContent {
                val user = cViewModel.userLiveData.observeAsState().value
                val qrValue = viewModel.qrisLiveData.observeAsState().value
                val trxData = viewModel.trxLiveData.observeAsState().value
                PayQrisTheme {
                    // A surface container using the 'background' color from the theme
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colors.background
                    ) {
                        Column(modifier = Modifier.fillMaxSize()) {
                            TopAppBar()
                            Column(horizontalAlignment = Alignment.CenterHorizontally,
                            modifier = Modifier
                                .fillMaxSize()
                                .padding(all = padding_large)) {
                                if (trxData != null) {
                                    Text("PEMBAYARAN QR KE", color = Grey_700)
                                    Spacer(modifier = Modifier.height(spacer_normal))
                                    Text("${trxData.merchantName}", style = Typography.subtitle1)
                                    Text("${trxData.trxId}")
                                    Spacer(modifier = Modifier.height(spacer_large))
                                    Text("Total Pembayaran", style = Typography.h6,
                                        modifier = Modifier.padding(vertical = padding_normal))
                                    Text("${trxData.nominal}", style = Typography.h4, color = Turquoise_700,
                                        modifier = Modifier.padding(vertical = padding_normal))
                                }
                                if (user != null) {
                                    Spacer(modifier = Modifier.height(spacer_normal))
                                    Text("Dari Rekening")
                                    Text("${user.rekening}", style = Typography.subtitle1)
                                }
                                Spacer(modifier = Modifier.weight(1f, true))
                                Row(horizontalArrangement = Arrangement.SpaceBetween) {
                                    Button(onClick = { findNavController().navigate(R.id.navHomeFragment) },
                                        colors = ButtonDefaults.buttonColors(backgroundColor = Grey_700),
                                        modifier = Modifier
                                            .weight(1f, true)
                                            .padding(horizontal = padding_normal)
                                        ) {
                                        Text("Batal", color = Ghost_White)
                                    }
                                    Button(onClick = { findNavController().navigate(R.id.navPinFragment) },
                                        colors = ButtonDefaults.buttonColors(backgroundColor = Turquoise_500),
                                        modifier = Modifier
                                            .weight(1f, true)
                                            .padding(horizontal = padding_normal)
                                    ) {
                                        Text("OK", color = Ghost_White)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @OptIn(ExperimentalFoundationApi::class)
    @Composable
    private fun TopAppBar() {
        TopAppBar(
            title = {
                Text(
                    text = ""
                )
            },
            backgroundColor = Color.Transparent,
            contentColor = Orange_700,
            navigationIcon = {
                IconButton(onClick = { findNavController().navigateUp() }) {
                    Icon(imageVector = Icons.Filled.ArrowBackIos, "Icon Button Back")
                }
            },
            elevation = 0.dp
        )
    }
}
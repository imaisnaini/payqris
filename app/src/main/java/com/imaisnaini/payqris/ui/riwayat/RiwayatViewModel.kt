package com.imaisnaini.payqris.ui.riwayat

import androidx.compose.runtime.MutableState
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.imaisnaini.payqris.data.entity.Transaction
import com.imaisnaini.payqris.data.repository.TransactionRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class RiwayatViewModel @Inject constructor(private val transactionRepository: TransactionRepository): ViewModel() {
    var trxLiveData = MutableLiveData<List<Transaction>>()

    private var errorMsg = MutableLiveData("")
    fun getErrorMsg() = errorMsg

    private var showDialog = MutableLiveData(false)
    fun getShowDialog() = showDialog
    fun dismisDialog(){showDialog.value = false}

    fun findAll(){
        Single.fromCallable { transactionRepository.findAll() }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe({
                if (it.success) {
                    trxLiveData.postValue(it.records)
                }else{
                    errorMsg.postValue(it.message)
                }
                showDialog.postValue(false)
            }, { t: Throwable? -> Timber.e(t) })
    }
}
package com.imaisnaini.payqris.ui.qris

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ChevronLeft
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.google.common.util.concurrent.ListenableFuture
import com.imaisnaini.payqris.R
import com.imaisnaini.payqris.ui.theme.*
import com.imaisnaini.payqris.util.BarCodeAnalyser
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

@AndroidEntryPoint
class ScanQrisFragment: Fragment() {
    val viewModel: QrisViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        checkAndRequestCameraPermission()

        return ComposeView(requireContext()).apply {
            setContent {
                val errMsg = viewModel.errorMessage.observeAsState()
                PayQrisTheme {
                    // A surface container using the 'background' color from the theme
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                    ) {
                        Column(verticalArrangement = Arrangement.Center,
                            horizontalAlignment = Alignment.CenterHorizontally,
                            modifier = Modifier.fillMaxSize()) {
                            Box(modifier = Modifier.size(img_xlarge)) {
                                CameraPreview(
                                    onSuccess = {
                                        if (viewModel.isQRValid(it ?: "")) {
                                            findNavController().navigate(R.id.navPaymentFragment)
                                        } else {
                                            Toast.makeText(
                                                requireContext(),
                                                "${errMsg.value}",
                                                Toast.LENGTH_SHORT
                                                ).show()
                                        }
                                    },
                                )
                            }
                            Spacer(modifier = Modifier.height(img_small))
                            Image(painter = painterResource(id = R.drawable.ic_qris_logo), contentDescription = "",
                            modifier = Modifier.height(img_small))
                        }
                    }
                }
            }
        }
    }


    @Composable
    fun CameraPreview(onSuccess: (value:String?) -> Unit,
                      value: MutableState<String> = remember {
                          mutableStateOf("")
                      }
    ) {
        val context = LocalContext.current
        val lifecycleOwner = LocalLifecycleOwner.current
        var preview by remember { mutableStateOf<Preview?>(null) }
        val barCodeVal = remember { mutableStateOf("") }

        AndroidView(
            factory = { AndroidViewContext ->
                PreviewView(AndroidViewContext).apply {
                    this.scaleType = PreviewView.ScaleType.FILL_START
                    layoutParams = ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT,
                    )
                    implementationMode = PreviewView.ImplementationMode.COMPATIBLE
                }
            },
            modifier = Modifier
                .fillMaxSize(),
            update = { previewView ->
                val cameraSelector: CameraSelector = CameraSelector.Builder()
                    .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                    .build()
                val cameraExecutor: ExecutorService = Executors.newSingleThreadExecutor()
                val cameraProviderFuture: ListenableFuture<ProcessCameraProvider> =
                    ProcessCameraProvider.getInstance(context)

                cameraProviderFuture.addListener({
                    preview = Preview.Builder().build().also {
                        it.setSurfaceProvider(previewView.surfaceProvider)
                    }
                    val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()
                    val barcodeAnalyser = BarCodeAnalyser { barcodes ->
                        barcodes.forEach { barcode ->
                            barcode.rawValue?.let { barcodeValue ->
                                barCodeVal.value = barcodeValue
                                value.value = barcodeValue
//                            Toast.makeText(context, barcodeValue, Toast.LENGTH_SHORT).show()
                                onSuccess(barcodeValue)
                            }
                        }
                    }
                    val imageAnalysis: ImageAnalysis = ImageAnalysis.Builder()
                        .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                        .build()
                        .also {
                            it.setAnalyzer(cameraExecutor, barcodeAnalyser)
                        }

                    try {
                        cameraProvider.unbindAll()
                        cameraProvider.bindToLifecycle(
                            lifecycleOwner,
                            cameraSelector,
                            preview,
                            imageAnalysis
                        )
                    } catch (e: Exception) {
                        Log.d("TAG", "CameraPreview: ${e.localizedMessage}")
                    }
                }, ContextCompat.getMainExecutor(context))
            }
        )
    }

    private fun checkAndRequestCameraPermission() {
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            Timber.i("CAMERA permission granted")

        } else if (ActivityCompat.shouldShowRequestPermissionRationale(
                requireActivity(),
                Manifest.permission.CAMERA
            )
        ) {
            Timber.i("Permission Rationale is enforced")
            com.google.android.material.snackbar.Snackbar.make(
                requireView(),
                "Izinkan aplikasi mengakses kamera untuk menjalankan Image Recognition",
                com.google.android.material.snackbar.Snackbar.LENGTH_INDEFINITE
            )
                .setAction("OK") { v: View? ->
                    Timber.i("Launch permission request screen after showing rationale")
                    cameraPermissionLauncher.launch(Manifest.permission.CAMERA)
                }
                .show()
        } else {
            Timber.i("Launch permission request screen")
            cameraPermissionLauncher.launch(Manifest.permission.CAMERA)
        }
    }

    val cameraPermissionLauncher = registerForActivityResult<String, Boolean>(
        ActivityResultContracts.RequestPermission()
    ) { isGranted: Boolean ->
        if (isGranted) {
            Timber.i("CAMERA permission granted")
            Toast.makeText(context, "Izin akses diberikan", Toast.LENGTH_SHORT).show()
        } else {
            //permission was denied
            Toast.makeText(context, "Izin akses ditolak..!", Toast.LENGTH_SHORT).show()
        }
    }
}
package com.imaisnaini.payqris

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.imaisnaini.payqris.data.entity.User
import com.imaisnaini.payqris.data.repository.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class CommonViewModel @Inject constructor(private val userRepository: UserRepository): ViewModel() {
    var userLiveData = MutableLiveData<User?>(null)

    private var errorMsg = MutableLiveData("")
    fun getErrorMsg() = errorMsg

    private var showDialog = MutableLiveData(false)
    fun getShowDialog() = showDialog
    fun dismisDialog(){showDialog.value = false}

    fun refresh(){
        Single.fromCallable { userLiveData.value?.let { userRepository.findByID(it.id) } }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe({
                if (it.success && it.record != null) {
                    userLiveData.postValue(it.record)
                    errorMsg.postValue("")
                }else{
                    errorMsg.postValue("Invalid username or pin!")
                    userLiveData.postValue(null)
                }
                showDialog.postValue(false)
            }, { t: Throwable? -> Timber.e(t) })
    }

    fun login(username:String, pin:Int){
        Single.fromCallable { userRepository.login(username, pin) }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe({
                if (it.success && it.record != null) {
                    userLiveData.postValue(it.record)
                    errorMsg.postValue("")
                }else{
                    errorMsg.postValue("Invalid username or pin!")
                    userLiveData.postValue(null)
                }
                showDialog.postValue(false)
            }, { t: Throwable? -> Timber.e(t) })
    }

    fun register(user: User){
        Single.fromCallable { userRepository.register(user) }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe({
                if (it.success && it.record != null) {
                    userLiveData.postValue(it.record)
                    errorMsg.postValue("")
                }else{
                    errorMsg.postValue("Invalid username or pin!")
                    userLiveData.postValue(null)
                }
                showDialog.postValue(false)
            }, { t: Throwable? -> Timber.e(t) })
    }
}
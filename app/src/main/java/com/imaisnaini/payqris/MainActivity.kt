package com.imaisnaini.payqris

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.consumeWindowInsets
import androidx.compose.ui.platform.ComposeView
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI.setupWithNavController
import com.imaisnaini.payqris.databinding.ActivityMainBinding
import com.imaisnaini.payqris.ui.theme.PayQrisTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private var binding: ActivityMainBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(
            layoutInflater
        )
        setContentView(binding!!.root)
        //commonViewModel.loadCameraProvider()
        setupNavigation()
    }

    /**
     * Connect BottomNavigationView to navigation graph
     */
    fun setupNavigation() {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.navHost) as NavHostFragment?
        val navController = navHostFragment!!.navController
        setupWithNavController(binding!!.bottomNav, navController)

        // handle show/hide BottomNavigationView based on destination
        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            if (destination.id == R.id.navHomeFragment || destination.id == R.id.navRiwayatFragment
                || destination.id == R.id.navScanFragment) {
                binding!!.bottomNav.visibility = View.VISIBLE
            } else {
                binding!!.bottomNav.visibility = View.GONE
            }
        }
    }
}